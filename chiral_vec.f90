program main
  implicit none
  real(kind=8) :: a, d, pi
  integer :: i,j,n,m
!(n,m)
pi=3.14159265359
a=0d0
!n=18
do n=0, 25 
  do m=0,25
   a = 57.2958*atan(dsqrt(3d0)*m/(m+2*n) )
   d =  (0.246*(n**2 + n*m +m**2)**(0.5))/pi

  write(*,*)'index(n,m)',n,m,'chiral Angle(deg)', a, 'diameter(nm)', d
  end do
enddo
end program main
